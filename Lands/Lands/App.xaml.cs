﻿

using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Lands
{

    using System;
    using Lands.Views;
    using Xamarin.Forms;


    public partial class App : Application
    {

        #region Properties
        
        public static NavigationPage Navigator
        {
          get;
          internal set; 
        }
        #endregion

        #region Constructor
        public App()
        {
            InitializeComponent();

            //MainPage = new MainPage();
            //this.MainPage = new NavigationPage(new LoginPage());
            this.MainPage = new MasterPage();
        } 
        #endregion

        #region Methods
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        } 


        #endregion
    }
}

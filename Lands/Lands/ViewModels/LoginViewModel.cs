﻿


namespace Lands.ViewModels
{

    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;
    using Lands.Services;
    using  Helpers;

    public class LoginViewModel : BaseViewModel
    {


        #region Services

        private ApiService apiService;


        #endregion

        #region Attributes
        private string email;
            private string password;
            private bool isRunning;
            private bool isEnabled;
        #endregion

        #region Properties
            public string Email
            {
                get { return this.email; }
                set { SetValue(ref this.email, value); }
            }

            public string Password
            {
                get { return this.password; }
                set { SetValue(ref this.password, value); }
            }

            public bool IsRunning
            {
                get { return this.isRunning; }
                set { SetValue(ref this.isRunning, value); }
            }

            public bool IsRemembered { get; set; }

            public bool IsEnabled
            {
                get { return this.isEnabled; }
                set { SetValue(ref this.isEnabled, value); }
            }
        #endregion

        #region Constructors

            public LoginViewModel()
            {
                this.apiService = new ApiService();
                this.IsRemembered = true;
                this.IsEnabled = true;

                this.email = "livingstone23@gmail.com";
                this.password = "123456";

                // http://restcountries.eu/rest/v2/all

            }


        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }


        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You must enter a email.", "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You must enter a password.", "Accept");
                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;

            //if (this.Email != "livingstone23@gmail.com" || this.Password != "1234")
            //{
            //    this.IsRunning = false;
            //    this.IsEnabled = true;

            //    await Application.Current.MainPage.DisplayAlert("Error", "Email or password incorrect", "Accept");
            //    this.Password = string.Empty;
            //    return;
            //}

            var connection = await this.apiService.CheckConnection();
            if(!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "Accept");
                this.Password = string.Empty;
                return;
            }

            var token = await this.apiService.GetToken("http://api.yunuxx.com", this.email, this.password);

            if (token == null)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error, 
                    Languages.EmailValidation, 
                    Languages.Accept);
                this.Password = string.Empty;
                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                this.IsRunning = false;
                this.IsEnabled = true;

                this.email = string.Empty;
                this.password = string.Empty;

                await Application.Current.MainPage.DisplayAlert("Error", token.ErrorDescription, "Accept");
                this.Password = string.Empty;
                return;
            }

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token;
            mainViewModel.Lands = new LandsViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new LandsPage());

            this.IsRunning = false;
            this.IsEnabled = true;

            await Application.Current.MainPage.DisplayAlert("Ok", "Bienvenido a JOIN", "Accept");
            this.email = string.Empty;

            this.password = string.Empty;
            this.email = string.Empty;

           
        }

        #endregion

    }
}
